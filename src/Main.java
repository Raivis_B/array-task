/*
Implement the following functionalities based on 100,000 element arrays with randomly selected values:
1. return a list of unique items,
2. return a list of elements that have been repeated at least once in the generated array,
3. return a list of the 25 most frequently recurring items.
Implement a method that deduplicates items in the list. If a duplicate is found, it replaces it with a
new random value that did not occur before. Check if the method worked correctly by calling method number 2. (edited)
 */

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        int[] randomValues = new int[100];
        for (int i = 0; i < randomValues.length; i++) {
            randomValues[i]=getRandom();
        }

        System.out.println(Arrays.toString(randomValues));
        System.out.println(getUniques(randomValues));
        System.out.println(getDuplicates(randomValues));
    }

    public static int getRandom(){
        Random random = new Random();
        int randomNumber = random.nextInt(100);
        return randomNumber;
    }

    public static String  getUniques(int[] randomValues){
        int[] uniques = Arrays.stream(randomValues).distinct().toArray();
        return Arrays.toString(uniques);
    }

    public static String  getDuplicates(int[] randomValues){
        ArrayList<Integer> list = IntStream.of(randomValues).boxed().collect(Collectors.toCollection(ArrayList::new));
        Set<Integer> duplicates = list.stream().filter(i -> Collections.frequency(list,i)>1).collect(Collectors.toSet());
        return "Ieraksti, kas atkārtojas: " + duplicates.toString();
    }



}
